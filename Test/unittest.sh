data="Examples/DMChallenges.xlsx"
tempfile="temp.txt"
idpfile="idptemp.txt"

# IDPZ3 CHECKS BASED ON XLSX AND XML ##############################################################
sheets=("Map_Coloring" "Monkey_Business" "Crack_the_Code" "Calculator" "Change_Making" "Classify_Department_Employees" "Customer_Greeting" "Covid_Testing" "Doctor_Planning" "Duplicate_Product_Lines" "EquationHaiku" "EquationHaiku_alt" "Nim_Rules" "Online_Dating" "Vacation_Days_alt" "Vacation_Days"
        "Map_Coloring_Violations" "Zoo_Buses_and_Kids" "Hamburger_Challenge" "Where_is_gold" "Who_Killed_Agatha" "Christmas_Model" "Smart_Investment" "Boat_Rental")
data="Examples/DMChallenges.xlsx"
for sheet in ${sheets[*]};  # Generate idpz3 of every working sheet.
do
    echo "z3:" $data "$sheet"
    python3 -O solver.py $data -n "$sheet" -o "./$sheet.idp" --idp-z3 --interactive-consultant > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Problem!"
        cat $tempfile
        exit 1
    fi
done

for sheet in ${sheets[*]};  # Generate json of every working sheet.
do
    echo "json:" $data "$sheet"
    python3 -O solver.py $data -n "$sheet" -o "./$sheet.idp" --to-json > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Problem!"
        cat $tempfile
        exit 1
    fi
done

for xml in ${xmls[*]};  # Generate idpz3 of every xml.
do
    echo "z3:" $xml
    python3 -O solver.py "./Examples/XML/$xml" -o "./$xml.idp" --idp-z3 > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Problem!"
        cat $tempfile
        exit 1
    fi
done

overlap_error=("food_duplicate.dmn" "food_duplicate2.dmn" "food_duplicate3.dmn")
output_error=("dish-decision" "dish-decision" "dish-decision")

for ((i=0;i<${#overlap_error[@]};++i));  # Generate idpz3 of every error checking xml
do
    xml="${overlap_error[i]}"
    overlap="${output_error[i]}"
    echo "z3:" $xml
    python3 -O solver.py "./Examples/XML/$xml" -o "./$xml.idp" --idp-z3 --errorcheck-overlap $overlap > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Problem!"
        exit 1
    fi
done

# Also check the set example
echo "Check set"
python solver.py Examples/set.xlsx --all-sheets --idp-z3 > $tempfile 2>&1
if grep -q "Error" $tempfile; then
    echo "Problem!"
    exit 1
fi

# Also check the other examples
sheets=("Programming" "Handyman")
for sheet in ${sheets[*]};
do
    echo "Check $sheet"
    python solver.py Examples/other.xlsx -n "Programming" --idp-z3 > $tempfile 2>&1
    if grep -q "Error" $tempfile; then
        echo "Problem!"
        exit 1
    fi
done
