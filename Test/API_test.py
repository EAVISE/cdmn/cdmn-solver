import sys
import os
sys.path.append(os.getcwd())

try:
    from cdmn.API import DMN

    # Test BMI case.
    spec = DMN('./Examples/XML/BMILevel.dmn')

    print("inputs:", spec.get_inputs())
    print("outputs:", spec.get_outputs())
    print("other:", spec.get_intermediary())
    print('Dependencies of BMI:', spec.dependencies_of('bmi'))
    print('Dependencies of riskLevel:\n', spec.dependencies_of('riskLevel'))
    print(spec.possible_values_of('riskLevel'))

    spec.set_value('BMILevel', 'Normal')
    spec.set_value('length', 1.70)

    print(spec.model_expand(1))
    print(spec.maximize('bmi'))

    print(spec.minimize('length'))

    spec = DMN('./Examples/XML/BMILevel.dmn')

    spec.set_value('riskLevel', 'Extremely_High')

    spec.propagate()
    assert spec.value_of('riskLevel') == 'Extremely_High', 'Assertion failed'
    # assert spec.value_of('BMILevel') == 'Extreme_Obesity', 'Assertion failed'

    spec.clear()
    spec.set_value('bmi', '25')
    spec.set_value('bmi', 25)
    spec.set_value('waist', 70)
    spec.set_value('sex', 'Female')
    sys.exit(0)
    # spec.propagate()

    # assert spec.value_of('BMILevel') == 'Overweight', 'Assertion failed'
    # assert spec.value_of('riskLevel') == 'Increased', 'Assertion failed'

    # Test Circuit case.
    spec = DMN('./Examples/XML/LogicCircuit.dmn', auto_propagate=True)
    spec.set_value('value_K', True)
    spec.set_value('value_L', True)
    spec.set_value('value_M', True)

    assert spec.value_of('value_R') is True, 'Assertion failed'
    assert spec.value_of('value_N') is False, 'Assertion failed'

    spec.clear()
    spec.set_value('value_N', False)
    assert spec.value_of('value_Q') is False, 'Assertion failed'

    spec.set_value('value_M', True)
    assert spec.value_of('value_P') is True, 'Assertion failed'

    spec.set_value('value_O', False)
    assert spec.value_of('value_R') is False, 'Assertion failed'

    spec.set_value('value_M', None)
    assert not spec.is_certain('value_P'), 'Assertion failed'

    spec.set_value('value_O', None)
    assert spec.value_of('value_Q') is False, 'Assertion failed'

    # Test Food Decision
    spec = DMN('./Examples/XML/food.dmn', auto_propagate=True)
    spec.set_value('temperature', 25)

    assert spec.value_of('season') == 'Spring', 'Assertion failed'
    spec.set_value('desiredDish', 'Stew')

    assert spec.value_of('guestCount') == '4', 'Assertion failed'

    # Test Tax
    spec = DMN('./Examples/XML/tax.dmn')
    print(spec.model_expand(5))

    # Test newer BMI case (using latest Camunda format).
    spec = DMN('./Examples/XML/BMILevel_03_05_2023.dmn')

    print("inputs:", spec.get_inputs())
    print("outputs:", spec.get_outputs())
    print("other:", spec.get_intermediary())
    print('Dependencies of BMI:', spec.dependencies_of('bmi'))
    print('Dependencies of riskLevel:\n', spec.dependencies_of('riskLevel'))
    print(spec.possible_values_of('riskLevel'))

    spec.set_value('BMILevel', 'Normal')
    spec.set_value('length', 1.70)

    print(spec.model_expand(1))
    print(spec.maximize('bmi'))

    print(spec.minimize('length'))

    spec = DMN('./Examples/XML/BMILevel.dmn')

    spec.set_value('riskLevel', 'Extremely_High')

    spec.propagate()
    assert spec.value_of('riskLevel') == 'Extremely_High', 'Assertion failed'
    assert spec.value_of('BMILevel') == 'Extreme_Obesity', 'Assertion failed'

    spec.clear()
    spec.set_value('bmi', '25')
    spec.set_value('bmi', 25)
    spec.set_value('waist', 70)
    spec.set_value('sex', 'Female')
    spec.propagate()

    assert spec.value_of('BMILevel') == 'Overweight', 'Assertion failed'
    assert spec.value_of('riskLevel') == 'Increased', 'Assertion failed'

    # Test Circuit case.
except Exception as e:
    print(e)
    sys.exit(1)
