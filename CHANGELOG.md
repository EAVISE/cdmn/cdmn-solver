# cDMN CHANGELOG

# Unreleased

Enhancements:
- MR 91: add option to export model to json, for use in IDP-Z3 webIDE and Interactive Consultant

# 2.1.2

## Enhancements:
- MR 86: accept Unicode in Excel files; use the value of the formula in a cell
- iss128: allow FO(.) assertions in table with "FO(.)" hit policy
- iss131: Optimize output when quantifying with singular value

## Bugs:
- iss136: Python3.12 install issue
- iss137: <= is not translated to =<
- iss138: Type with space is not discovered in predicate/function declarations
- iss139: incorrect interpretation  due to nested symbol

# 2.1.1

## Breaking:
- (BREAKING) iss115: Data tables now use hit policy D instead of the `Data Table` key word in their name.

## Enhancements:
- iss125: Use Levenshtein distance for better interpretation matching
- iss119: Update to latest IDP-Z3 version (0.10.11)
- iss107, iss110: fix PLY warnings
- Added support for annotations in decision and constraint tables.
- iss118: Support latest Camunda XML
- Added a dedicated `to_fodot` function, to make integrating cDMN easier.

## Documentation:
- iss108, iss114, iss116, iss117: minor docs improvements

# 2.0.0

Release of version 2.0.0! Beware: this version introduces some breaking changes.

## Breaking:
- iss92: Support for IDP3 was dropped. Code was refactored to reflect this change, and many CLI flags have also been altered accordingly.

## Enhancements:
- iss76: Re-implemented the version flag in the CLI.
- iss95: IDP-Z3's optimization can now be used.


## Bugs:
- iss100: Fix bug where cdmn introduces a term block for IDP-Z3

## Documentation:
- iss89: Added information on propagation in the docs
- iss101: Updated the list of working DM Challenges
- iss102: Merged the documentation in the main cDMN repo
- iss103: Optimized the DM Challenge implementations for IDP-Z3, now that IDP3 has been dropped.
