# Script to compile Sphinx and then reload the Firefox page.
#!/bin/bash

# Compile Sphinx
make html

# Reload FF
CURRENT_WID=$(xdotool getwindowfocus)

WID=$(xdotool search --name "Mozilla Firefox")
xdotool windowactivate $WID
xdotool key F5

xdotool windowactivate $CURRENT_WID
