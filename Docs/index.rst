.. Author: Simon Vandevelde

Welcome to the cDMN documentation!
===================================

.. role:: raw-html(raw)
    :format: html

1. What is cDMN?
--------------------

*Constraint Decision Modeling and Notation* (or cDMN for short) is an extension to the `DMN <https://www.omg.org/dmn/>`_ standard, which is managed by the Object Management Group (OMG).
An explanatory summary of DMN is listed below. 

.. admonition:: From the DMN website:

    DMN is a modeling language and notation for the precise specification of business decisions and business rules.
    DMN is easily readable by the different types of people involved in decision management.
    These include: business people who specify the rules and monitor their application; business analysts.

DMN models are created by chaining together **decisions**.
Every decision is defined by a **decision table**.
For example, the table below shows how to decide the dish to cook based on the season and the amount of guests.
Every decision table has input variables, and an output variable.
Each row of a table denotes a possible combination of input variables, which define one or more outputs.
This way, decisions can be represented in a readable manner.
If for example Season is Winter and there are 10 guests, row 2 dictates that the Dish is Pasta.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Decide what to eat</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Season</td>
            <td class="dec-input">NumberOfGuests</td>
            <td class="dec-output">Dish</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Winter</td>
            <td class="dec-td">&le; 8</td>
            <td class="dec-td">Spareribs</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Winter</td>
            <td class="dec-td">&gt; 8</td>
            <td class="dec-td">Pasta</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Summer</td>
            <td class="dec-td">&gt; 10</td>
            <td class="dec-td">Light Salad</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Summer</td>
            <td class="dec-td">&le; 10</td>
            <td class="dec-td">Beans Salad</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">Spring</td>
            <td class="dec-td">&lt; 10</td>
            <td class="dec-td">Stew</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">Spring</td>
            <td class="dec-td">&ge; 10</td>
            <td class="dec-td">Steak</td>
        </tr>
    </table>
    <br>

cDMN extends standard DMN by adding constraint reasoning, variable quantification, more expressive data representations, and more.
Because of these additions, cDMN has a couple advantages over DMN.

    * Constraints increase expressivity and flexibility.
    * Models scale better with input size.
    * Models are more compact and straightforward.
    * Modelling complex systems becomes possible.

cDMN is part of ongoing research by `Simon Vandevelde <https://simonvandevelde.be>`_ and `Joost Vennekens <https://people.cs.kuleuven.be/~joost.vennekens/>`_ at the `EAVISE group <https://iiw.kuleuven.be/onderzoek/eavise>`_, KU Leuven University.
If you are interested in learning more about how cDMN could help you and/or your company, `contact us <s.vandevelde@kuleuven.be>`_.

For those who want to read more about the theoretical side of cDMN, you can read our paper titled `"Tackling the DM Challenges with cDMN: a Tight Integration of DMN and constraint reasoning" <https://arxiv.org/pdf/2110.02610.pdf>`_.

cDMN has also been presented at two conferences: the more theoretical RuleML+RR2020 presentation can be viewed `here <https://www.youtube.com/watch?v=Nithms7HYmI>`_, and the more practical DecisionCAMP20 presentation can be viewed `on their youtube <https://www.youtube.com/watch?v=KCqxJVKTZ-U>`_.

2. How do I use it?
-------------------


Notation
~~~~~~~~

The :ref:`notation` page details all cDMN features and how to use them.
If you are interested in learning cDMN, it is recommended to read/skim through this page, and then view the concrete cDMN examples to see cDMN in action.

Using the notation is straightforward: models can be created in any spreadsheet editing software you like.
If you wish to execute your models, you can use our :ref:`solver`.
Although, keep in mind that our solver currently only supports cDMN models in the :code:`.xlsx` format.

Installation
~~~~~~~~~~~~

.. note:: We are working on a tool that lets you interact with cDMN directly, without installation. `A preview version is available for free <http://cdmn-editor.simonvandevelde.be/>`_.

The cDMN solver consists of a converter written in Python 3, and an off-the-shelf knowledge reasoning engine (`IDP-Z3 system <https://www.IDP-Z3.be>`_).
Information on the installation of the cDMN solver can be found here: :ref:`solver`.
The solver fully works on Windows, Mac and Linux.
Furthermore, we are also planning on creating a browser-based tool, which everyone could run, regardless of OS.


Usage
~~~~~

More information on the usage of the cDMN solver can be found at :ref:`solver_usage`.

Examples
~~~~~~~~

For examples, see the :ref:`examples` page.

3. Can I also use it as a tool for standard DMN?
------------------------------------------------

Yes you can!
The cDMN solver supports everything in the normal DMN standard, besides boxed expressions and tables with the *C* hit policy.
All other hit policies (*U*, *A*, *F*, *C#*, ...) are supported.
The DMN specification can be either be in the form of a spreadsheet, or in the XML format as specified in the standard.
An explanation on how to use cDMN for DMN is given at the :ref:`solver_usage` page.

On top of our cDMN tool, we also bundle a Python DMN API, which serves as an embeddable reasoning engine.
This API allows for querying the DMN specification for information (such as input names, output names, ...), and allows for setting variable values and propagating them throughout the model..
A guide on the usage of the API can be found at :ref:`dmn_guide`.

We also have a tool which integrates a DMN editor with an IDP-based user-friendly interface, called DMN-IDP.
You can try it out for yourself at `the online DMN-IDP demo <https://dmn-idp.onrender.com/>`_.

4. How to contribute
--------------------

cDMN's source code is hosted on `this GitLab repo <https://gitlab.com/EAVISE/cdmn/cdmn-solver>`_ under the GNU GPLv3 license.
There's also documentation available for developers, over at :ref:`api`.


5. How to reference
-------------------

If you used cDMN in a publication or in other works, please reference us as follows:

BibTeX:

.. code::

    @article{cDMN,
      title = {Tackling the {{DM}} Challenges with {{cDMN}}: {{A}} Tight Integration of {{DMN}} and Constraint Reasoning},
      author = {VANDEVELDE, SIMON and AERTS, BRAM and VENNEKENS, JOOST},
      year = {2021},
      journal = {Theory and Practice of Logic Programming},
      pages = {1--24},
      publisher = {{Cambridge University Press}},
      doi = {10.1017/S1471068421000491}
    }

Direct cite:

.. code::

   VANDEVELDE, S., AERTS, B., & VENNEKENS, J. (2021). Tackling the DM Challenges with cDMN: A Tight Integration of DMN and Constraint Reasoning. Theory and Practice of Logic Programming, 1-24. doi:10.1017/S1471068421000491

 
.. toctree::
   :maxdepth: 3
   :caption: Full Documentation Contents: 

   notation
   solver
   examples
   cdrd
   DMN_guide
   conversion
   api
   pdmn
