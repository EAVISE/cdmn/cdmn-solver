.. _hamburger_challenge:

Hamburger Challenge
-------------------

This example is also taken from `dmcommunity.org <https://dmcommunity.org/>`_.
It's called `Make a Good Burger <https://dmcommunity.org/challenge/make-a-good-burger/>`_.

.. admonition:: Hamburger Challenge


    A burger most include at least one of each item, and no more than five of each item.
    You must use whole items (for example, no half servings of cheese).
    The final burger must contain less than 3000 mg of sodium, less than 150 grams of fat, and less than 3000 calories.
    To maintain certain taste quality standards, you'll need to keep the servings of ketchup and lettuce the same.
    Also, you'll need to keep the servings of pickles and tomatoes the same.
    Below is a list of ingredients and their information.

    **Try to find the most, and least expensive burger possible using the following items.**

.. csv-table::
    :header: "Item", "Sodium (mg)", "Fat (g)", "Calories", "Cost"

    "Beef Patty", "50", "17", "220", "0.25"
    "Bun", "330", "9", "260", "0.15"
    "Cheese", "310", "6", "70", "0.10"
    "Onions", "1", "2", "10", "0.09"
    "Pickles", "260", "0", "5", "0.03"
    "Lettuce", "3", "0", "4", "0.04"
    "Ketchup", "160", "0", "20", "0.02"
    "Tomato", "3", "0", "9", "0.04"

As always, we start by filling out the glossary.
From the description of the problem, we immediately know that we will need a type for the items, so we introduce ``Item``.
Because having to type over every item in the ``Values`` column is cumbersome, we refer to the data table in which we will be writing down all the food names.
This ensures that the list of items is filled automatically based on which items appear in the data table.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Item</td>
            <td class="glos-td">String</td>
            <td class="glos-td">See Data Table Nutritions</td>
        </tr>
    </table>
    <br>


Next up, we need a way to assign each item their nutritional information.
Since each item can only have one value for sodium, fat, calories and cost, we can model these best as functions!

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Sodium of Item</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Fat of Item</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Calories of Item</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Cost of Item</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Number of Item</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

We also need a way to find out the total amount of each nutritional value.
Since there is only one burger, and only one total amount per attribute, we use constants here.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Total Sodium</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Total Fat</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Total Calories</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Total Cost</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

Now that our glossary is done, we can move on to the next step in modeling the challenge: the data.

.. admonition:: Data

    **Inputting the nutritional values**

In our glossary entry for ``Item``, we wrote ``see DataTable Nutritions``.
This tells the cDMN solver to add all the values in the ``Item`` column of the data table to the list of possible values.
Creating the data table is easy.
We have one input column, ``Item``, and 4 output columns (one for every nutritional value).
This greatly shows one of the advantages of data tables: if the list of nutritional values is supplied in table format or as a CSV, all we need to do is copy and paste the values and change the header names.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Nutritions</th>
            <th class="dec-empty" colspan="4">D</th>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-input">Item</td>
            <td class="dec-output">Sodium of Item</td>
            <td class="dec-output">Fat of Item</td>
            <td class="dec-output">Calories of item</td>
            <td class="dec-output">Cost of Item</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Beef Patty</td>
            <td class="dec-td">50</td>
            <td class="dec-td">17</td>
            <td class="dec-td">220</td>
            <td class="dec-td">25</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Bun</td>
            <td class="dec-td">330</td>
            <td class="dec-td">9</td>
            <td class="dec-td">260</td>
            <td class="dec-td">15</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Cheese</td>
            <td class="dec-td">310</td>
            <td class="dec-td">6</td>
            <td class="dec-td">70</td>
            <td class="dec-td">10</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Onions</td>
            <td class="dec-td">1</td>
            <td class="dec-td">2</td>
            <td class="dec-td">10</td>
            <td class="dec-td">9</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">Pickles</td>
            <td class="dec-td">260</td>
            <td class="dec-td">0</td>
            <td class="dec-td">5</td>
            <td class="dec-td">3</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">Lettuce</td>
            <td class="dec-td">3</td>
            <td class="dec-td">0</td>
            <td class="dec-td">4</td>
            <td class="dec-td">4</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">Ketchup</td>
            <td class="dec-td">160</td>
            <td class="dec-td">0</td>
            <td class="dec-td">20</td>
            <td class="dec-td">2</td>
        </tr>
        <tr>
            <td class="dec-td">8</td>
            <td class="dec-td">Tomato</td>
            <td class="dec-td">3</td>
            <td class="dec-td">0</td>
            <td class="dec-td">9</td>
            <td class="dec-td">4</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule

    **The burger must contain at least one of each, and no more than five of each item.**

This rule can be modeled as a simple constraint table.
We evaluate every item, and specify that their amount is in the range of 1 to 5.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Number Constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Item</td>
            <td class="dec-output">Number of Item</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[1, 5]</td>
        </tr>
    </table>
    <br>


.. admonition:: Rule

    **The amount of lettuce and ketchup is the same, as well as pickles and tomatoes.**

Once again, a contraint table suffices.
In fact, we can even reuse our previous table by adding two rows.
We evaluate each item of lettuce, and make sure that their amount is the same as the amount of ketchup, and repeat this for the pickles and the tomatoes.
We could even add these rules to the previous table, since the two rules use the same input and output column.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Number Constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Item</td>
            <td class="dec-output">Number of Item</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[1, 5]</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Lettuce</td>
            <td class="dec-td">Number of Ketchup</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Pickles</td>
            <td class="dec-td">Number of Tomato</td>
        </tr>
    </table>
    <br>

.. admonition:: Rule
    
    **The burger must contain less than 3000mg of sodium, less than 150g of fat and less than 3000 calories.**

This rule is a bit more complex, because we need to calculate these total values first.
For each item, we calculate their number multiplied by their nutritional value and add all of those up to get the total, using the ``C+`` hit policy.
Then we use another ``E*`` to make sure we don't cross the nutritional thresholds.
Note how this second table doesn't have any inputcolumns.
Because we use constants, no inputs are needed.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Determine Nutrition</th>
            <th class="dec-empty" colspan="4"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Item</td>
            <td class="dec-output">Total Sodium</td>
            <td class="dec-output">Total Fat</td>
            <td class="dec-output">Total Calories</td>
            <td class="dec-output">Total Cost</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Number of Item * Sodium of Item</td>
            <td class="dec-td">Number of Item * Fat of Item</td>
            <td class="dec-td">Number of Item * Calories of Item</td>
            <td class="dec-td">Number of Item * Cost of Item</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Nutrition Constraints</th>
            <th class="dec-empty" colspan="3"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">Total Sodium</td>
            <td class="dec-output">Total Fat</td>
            <td class="dec-output">Total Calories</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&lt 3000</td>
            <td class="dec-td">&lt 150</td>
            <td class="dec-td">&lt 3000</td>
        </tr>
    </table>
    <br>
    

.. admonition:: Rule

    **Find the most expensive burger.**

To maximize the total cost, we can add a ``Goal`` table.
In this table, we need to specify the term we want to minimize, which in this case, is ``Total Cost``.

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Minimize Total Cost</td>
        </tr>
    </table>
    <br>




We can now run our model using the cDMN solver.
This results in the following output:

.. code:: bash

   Model 1
   ==========
   sodium_of_Item := {Beef_Patty->50, Bun->330, Cheese->310, Onions->1, Pickles->260, Lettuce->3, Ketchup->160, Tomato->3}.
   fat_of_Item := {Beef_Patty->17, Bun->9, Cheese->6, Onions->2, Pickles->0, Lettuce->0, Ketchup->0, Tomato->0}.
   calories_of_Item := {Beef_Patty->220, Bun->260, Cheese->70, Onions->10, Pickles->5, Lettuce->4, Ketchup->20, Tomato->9}.
   cost_of_Item := {Beef_Patty->25, Bun->15, Cheese->10, Onions->9, Pickles->3, Lettuce->4, Ketchup->2, Tomato->4}.
   number_of_Item := {Beef_Patty->1, Bun->1, Cheese->1, Onions->1, Pickles->1, Lettuce->1, Ketchup->1, Tomato->1}.
   Total_Sodium := 1117.
   Total_Fat := 34.
   Total_Calories := 598.
   Total_Cost := 72.

   Elapsed Time:
   0.890

We are able to make a burger which only costs 72, and still meets (meats? :-) ) all the requirements.
