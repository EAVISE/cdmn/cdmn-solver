.. _smart_investment:

Smart Investment
----------------

In the `smart investment challenge <https://dmcommunity.org/challenge-july-2024/>`_, taken from the DMCommunity, we should pick out a set of stocks to buy to maximize their predicted gain within one year.
The full problem is as follows:

.. admonition:: Smart Investment

   A client of an investment firm has $10000 available for investment.
   He has instructed that his money be invested in particular stocks, so that no more than $5000 is invested in any one stock but at least $1000 be invested in each stock.
   He has further instructed the firm to use its current data and invest in the manner that maximizes his overall gain during a one-year period.
   The stocks, the current price per share and the firm’s predicted stock price a year from now are summarized below:


    .. raw:: html 

        <table>
            <tr>
                <th>Stock</th>
                <th>Current Price</th>
                <th>Projected Price 1 year</th>
            </tr>
            <tr>
                <td>ABC</td>
                <td>25</td>
                <td>35</td>
            </tr>
            <tr>
                <td>XYZ</td>
                <td>50</td>
                <td>60</td>
            </tr>
            <tr>
                <td>TTT</td>
                <td>100</td>
                <td>125</td>
            </tr>
            <tr>
                <td>LMN</td>
                <td>25</td>
                <td>40</td>
            </tr>
        </table> 



As always, we start by creating our glossary.
In this case, we will firstly want a way to talk about the four kinds of stocks: for this, we introduce a *type*.
Recall that a type represents a domain of values, which we can use to express more complex variables.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Stock</td>
            <td class="glos-td">String</td>
            <td class="glos-td">ABC, XYZ, TTT, LMN</td>
        </tr>
    </table>
    <br>

Looking closer at the problem text, we can see that we need to represent five pieces of information for each stock:

* Their current price
* Their projected price
* Their predicted gain
* How many we buy
* How much they are worth in total

All five of these can be excellently represented using *functions*.
A function represents a mapping from one or more types on another type, which in this case is useful to map each Stock element on a different type as follows.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">current price of Stock</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">projected price of Stock</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Stock gain</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">nb Stock bought</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">dollars of Stock bought</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>


Nearly done! All that's left to express are a variable to represent how much money we start with, and a variable to represent the total worth after a year.
For this, we will rely on constants: these are functions, but without an input type.
As such, they represent exactly *one* value.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">total invested</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">projected total</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>


That's it! Now, we can start modelling the problem logic.
Luckily, these tables are fairly simple.
We start with two decision tables, to define the total value of the stock we bought and to define the predicted stock gain after one year.
As a reminder, the column "Stock = -" should be read as "For every stock".

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Define total value</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">dollars of Stock bought</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">nb Stock bought * current price of Stock</td>
        </tr>
    </table>
    <br>

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Define stock gain</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">Stock gain</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">projected price of Stock - current price of Stock</td>
        </tr>
    </table>
    <br>


Next, we want to express that the total value of each stock should be between 1000 and 5000 dollars.
This is easy to do using a constraint table (denoted by hit policy *E\**):

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Min and max stock value </th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">dollars of Stock bought</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">[1000, 5000]</td>
        </tr>
    </table>
    <br>

Here, the constraint table allows us to specify that the value should be in a specific range, without having to specify a concrete value.
In contrast, decision tables *always* need to define a single value.

For the last part of logic, we still need to express how to define how to calculate the total invested and the projected total.
For this, we use decision tables using the sum hit policy *C+*.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Count invested</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">total invested</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">dollars of Stock bought</td>
        </tr>
    </table>
    <br>
    
    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Projected gain</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">projected total</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">nb Stock bought * Stock gain</td>
        </tr>
    </table>
    <br>

Basically, these first table sums the value of each stock, while the second table sums the number of each stock multiplied by its predicted gain.

Now we're nearly done: all that's left is representing the input data (our budget and the stock info), and what we want the cDMN solver to calculate.
For the former, we use cDMN data tables (denoted by *D* hit policy) as follows.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Stock Data</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Stock</td>
            <td class="dec-output">current price of Stock</td>
            <td class="dec-output">projected price of Stock</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">ABC</td>
            <td class="dec-td">25</td>
            <td class="dec-td">35</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">XYZ</td>
            <td class="dec-td">50</td>
            <td class="dec-td">60</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">TTT</td>
            <td class="dec-td">100</td>
            <td class="dec-td">125</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">LMN</td>
            <td class="dec-td">25</td>
            <td class="dec-td">40</td>
        </tr>
    </table>
    <br>

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Budget</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-output">total invested</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">1000</td>
        </tr>
    </table>
    <br>

For the latter, we add a goal table to indicate to the cDMN solver that we want to find the maximum value for the projected total.

.. raw:: html
   
    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Maximize projected total </td>
        </tr>
    </table>
    <br>


Done! 
We can now run our model using the cDMN solver, which will tell us that 120 BAC, 20 XYZ, 10 TTT and 200 LMN will lead to an optimal projected total of 4650. :-) 
Not bad for one year!

.. code:: bash

    Model 1
    ==========
    current_price_of_Stock := {ABC -> 25, XYZ -> 50, TTT -> 100, LMN -> 25}.
    projected_price_of_Stock := {ABC -> 35, XYZ -> 60, TTT -> 125, LMN -> 40}.
    nb_Stock_bought := {ABC -> 120, XYZ -> 20, TTT -> 10, LMN -> 200}.
    total_invested := 10000.
    projected_total := 4650.
    dollars_of_Stock_bought := {ABC -> 3000, XYZ -> 1000, TTT -> 1000, LMN -> 5000}.
    Stock_gain := {ABC -> 10, XYZ -> 10, TTT -> 25, LMN -> 15}.

    Elapsed Time:
    0.574s
