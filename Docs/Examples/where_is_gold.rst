.. _where_is_gold:

Where is Gold?
--------------

This is an implementation for the `June 2021 DMCommunity challenge <https://dmcommunity.org/challenge/challenge-june-2021/>`_.
The challenge is as follows:

.. admonition:: Where is Gold?

    There are three boxes, but only one of them has gold inside.
    Additionally, each box has a message printed on it.
    One of these messages is true and the other two are lies.

    * The first box says, “Gold is in this box”.
    * The second box says, “Gold is not in this box”.
    * The third box says, “Gold is not in Box 1”.

    Which box contains the gold?


As always with the DMCommunity challenges, this is quite a fun one to implement.
We start by building our glossary.
It is clear that we need a way to reason over boxes, so we first introduce a type `Box`.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Box</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[1..3]</td>
        </tr>
    </table>
    <br>

Next, we need a way to express what boxes "speak the truth", and what boxes "lie".
Because the boolean nature of truth/lie, we can use a relation (representing `true`/`false` for every box's statement).


.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Box speaks truth</td>
        </tr>
    </table>
    <br>


To express what box contains the gold, we can make perfect use of a constant, `GoldBox`.
Similarly, to express the number of boxes speaking the truth, we introduce a constant `NbTruth`.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">GoldBox</td>
            <td class="glos-td">Box</td>
        </tr>
        <tr>
            <td class="glos-td">NbTruth</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

With our glossary finished, we continue to our decision/constraint tables.
We start by defining when a box speaks the truth.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Box speaks truth</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Box called b</td>
            <td class="dec-input">GoldBox</td>
            <td class="dec-output">b speaks truth</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">1</td>
            <td class="dec-td">1</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">2</td>
            <td class="dec-td">not(2)</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">3</td>
            <td class="dec-td">not(1)</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

The rules of the table can be read as follows:

* For box 1: if it contains the gold, it speaks the truth.
* For box 2: if it does not contain the gold, it speaks the truth.
* For box 3: if box 1 does not contain the gold, it speaks the truth.

We now count how many boxes speak the truth, in order to then create a constraint on it.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Count NbTruth</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Box called b</td>
            <td class="dec-input">b speaks truth</td>
            <td class="dec-input">NbTruth</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Truth</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">NbTruth</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>
    
The first table expresses that "For every box `b`, if it speaks the truth, add 1 to `NbTruth`".
The second table then expresses a constraint that `NbTruth` should always be 1, i.e., only one box may speak the truth.

With those two tables created, we are done!
We can now run our model using the cDMN solver, to find out which box contains the gold.

.. code:: bash

    Model 1
    ==========
    GoldBox := 2.
    NbTruth := 1.
    Box_speaks_truth := {3}.

    Elapsed Time:
    0.803
