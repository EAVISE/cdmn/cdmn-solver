.. _change_making:

Change Making
=============

This page details the cDMN implementation of the Change Making challenge, as listed on https://dmcommunity.org/challenge/challenge-feb-2015/.
The challenge is as follows:

.. admonition:: Change Making

   Let S be a given sum that we want to achieve with a minimal amount of coins in denominations of x1, x2, ..., xn.
   Here is a simple example: S is 123 cents, n is 4, x1 is 1 cent, x2 is 10 cents, x3 is 25 cents and xn is 100 cents.

This is a pretty cool example, as it shows the mathematical power of the cDMN solver.
It is also a great example to show how cDMN is able to optimize values.

We start by making the glossaries.
For this example, we do not need to introduce any types: all variables represent integer numbers, so we can rely on the built-in ``Int`` instead.

To represent the number of coins, we will use a constant for each denomination.
We will also use a constant to keep track of the total amount of coins, and the total amount of money.


.. raw:: html

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">TotalMoney</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TotalCoins</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">OneCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TwoCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">FiveCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TenCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TwentyCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">FiftyCent</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">OneEuro</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TwoEuro</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

Now that our glossary is finished, we can move on to the decision tables.
We will be creating three tables which consist exclusively of output columns.

The first one is the total amount of money which we want to find the optimal assignment for.
This is easily represented as:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Total</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">TotalMoney</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">123</td>
        </tr>
    </table>
        <br>
            
Next up, we want a way to represent the following formula: ``TotalMoney = OneCent * 1 + TwoCent * 2 + ...``.
Using a decision table with the ``C+`` hit policy, we can model this as:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Change</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">TotalMoney</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">OneCent * 1</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">TwoCent * 2</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">FiveCent * 5</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">TenCent * 10</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">TwentyCent * 20</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">FiftyCent * 50</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">OneEuro * 100</td>
        </tr>
        <tr>
            <td class="dec-td">8</td>
            <td class="dec-td">TwoEuro * 200</td>
        </tr>
    </table>
    <br>

Similarly, to count the amount of total coins:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Coins</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-output">TotalCoins</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">OneCent</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">TwoCent</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">FiveCent</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">TenCent</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">TwentyCent</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">FiftyCent</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">OneEuro</td>
        </tr>
        <tr>
            <td class="dec-td">8</td>
            <td class="dec-td">TwoEuro</td>
        </tr>
    </table>
    <br>


If we were to run the current implementation, we would encounter something very curious: the solver would be able to find a solution in which exactly one coin is used. 
Indeed, we have not yet specified that our number of coins must always be positive, so the solver can e.g. find solutions with -3 TwoCent coins.
To overcome this, we add a constraint table as follows:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Coins</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-output">TotalCoins</td>
            <td class="dec-output">OneCent</td>
            <td class="dec-output">TwoCent</td>
            <td class="dec-output">FiveCent</td>
            <td class="dec-output">TenCent</td>
            <td class="dec-output">TwentyCent</td>
            <td class="dec-output">FiftyCent</td>
            <td class="dec-output">OneEuro</td>
            <td class="dec-output">TwoEuro</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
            <td class="dec-td">&ge; 0</td>
        </tr>
    </table>
    <br>

Now all that is left to do, is specify what we want to do with this model.
There are multiple solutions possible, but we want the solution with the optimal amount of coins
By creating a ``Goal`` table, we can tell the system to optimize the TotalCoins constant.


.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Minimize TotalCoins </td>
        </tr>
    </table>
    <br>

And that's it! With just 2 glossary tables, 3 decision tables and a ``Goal`` table, we were able to model an optimal change making algorithm.
If we run this in the cDMN solver, we get the following output:

.. code:: bash

   Model 1
   ==========
   TotalMoney := 123.
   TotalCoins := 4.
   OneCent := 1.
   TwoCent := 1.
   FiveCent := 0.
   TenCent := 0.
   TwentyCent := 1.
   FiftyCent := 0.
   OneEuro := 1.
   TwoEuro := 0.

   Elapsed Time:
   1.106

If we want to look for the optimal way to form 567 cents, the cDMN solver outputs the following:

.. code:: bash

   Model 1
   ==========
   TotalMoney := 567.
   TotalCoins := 7.
   OneCent := 0.
   TwoCent := 1.
   FiveCent := 1.
   TenCent := 1.
   TwentyCent := 0.
   FiftyCent := 1.
   OneEuro := 1.
   TwoEuro := 2.

   Elapsed Time:
   1.121

