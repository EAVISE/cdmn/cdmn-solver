.. _covid_testing:

Covid Testing
-------------

"Covid Testing" is the May 2021 `DMCommunity challenge <https://dmcommunity.org/challenge/challenge-may-2021/>`_, and is based on a Covid triage procedure that was at one point used by the Belgian government.

.. admonition:: Covid Testing

    We ask to submit a knowledge specification, through DM tables or otherwise, that describes the following triage procedure that determines for (a group of) patients who should undergo subsequent testing:

    1. Patients present sneezing and/or coughing as symptoms; this is determined by what the patient him or herself details (i.e., anamnesis).
    2. A patient’s temperature is measured. The default fever threshold is 38°C; due to different methods of measurement for young children and their associated accuracy, for patients younger than 10 the fever threshold is considered to be 37.2°C.

    Based on the first two steps, the number of presented symptoms is determined. If a patient presents at least two (2) of these symptoms, subsequent testing is performed, otherwise standard quarantine practices are advised.

    In this knowledge specification, pay special attention to the nature of the pandemic as an ongoing crisis with expertise being gained continuously. Specifically, take into account how the following scenarios impact your specification:

    The possible emergence of additional symptoms such as loss of smell or loss of taste.
    A different threshold of symptoms deemed present before referral to subsequent testing.


This challenge does actually not require any of the new concepts that cDMN brings to the table -- as such, the implementation will consist purely of standard DMN.

First, we start by building the glossary.
Just from reading the challenge description, it is clear that we need ways to represent the following things:

* Age
* Temperature
* The number of total symptoms
* Whether a person suffers from coughing
* Whether a person suffers from sneezing
* Whether a person suffers from fever
* Whether a PCR test is advisable

The first three concepts can easily be represented using constants, while the latter four concepts are perfectly fit to be represented by boolean variables.
Note that no type glossary is needed, because all types are either `int` or `real`.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">Age</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Temperature</td>
            <td class="glos-td">Float</td>
        </tr>
        <tr>
            <td class="glos-td">NbSymptoms</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Boolean</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Coughing</td>
        </tr>
        <tr>
            <td class="glos-td">Sneezing</td>
        </tr>
        <tr>
            <td class="glos-td">Fever</td>
        </tr>
        <tr>
            <td class="glos-td">PCRTest</td>
        </tr>
    </table>
    <br>

Using this small glossary, we will be able to model the challenge.
First, we should decide if a patient has a fever.

.. admonition:: Rule

   The default fever threshold is 38°C; due to different methods of measurement for young children and their associated accuracy, for patients younger than 10 the fever threshold is considered to be 37.2°C.

Using an F-table, we can elegantly model this rule.
Note that even though a U-table could also have been used, we chose for an F-table because it better fits the *default reasoning* used in the rule.
By default reasoning, we mean logic that consists of a set of IF-ELSE sentences, followed by a final ELSE statement.
E.g., we can rewrite the above statement to: "If a patient is younger than 10 and has a temperature higher than 37.2, they have a fever. Else if the patient is older than 10 and has a temperature higher than 38, they also have a fever. Otherwise, they have no fever."

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Fever</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">F</td>
            <td class="dec-input">Age</td>
            <td class="dec-input">Temperature</td>
            <td class="dec-output">Fever</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&lt 10</td>
            <td class="dec-td">&gt 37.2</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">&ge; 10</td>
            <td class="dec-td">&gt 38</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>

Next, we introduce a table to count the number of symptoms.
Instead of using a count table, we instead opt for a sum table, as this will allow us to use weighted sums in the case some symptoms become more important than others.
Another advantage of using this table structure is that adding new symptoms in the future will be easy.
Indeed, only 1 row and 1 column need to be added, consisting mostly of cells containing "-".

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Count Symptoms</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Sneezing</td>
            <td class="dec-input">Coughing</td>
            <td class="dec-input">Fever</td>
            <td class="dec-output">NbSymptoms</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">-</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

And finally, we add a table to define if a PCR Test is necessary.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">PCRtest</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">NbSymptoms</td>
            <td class="dec-output">PCRtest</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&ge; 2</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">&lt 2</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>


By now also adding a data table, we can enter patient data and use the model to find out if they require further PCR testing.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Patient</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-output">Age</td>
            <td class="dec-output">Temperature/td>
            <td class="dec-output">Coughing</td>
            <td class="dec-output">Sneezing</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">18</td>
            <td class="dec-td">38.2</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.

.. code:: bash

   Model 1
   ==========
   Temperature:={->38.2}
   Age:={->18}
   NbSymptoms:={->2}
   Sneezing:={->false}
   Coughing:={->true}
   Fever:={->true}
   PCRTest:={->true}
   Quarantine:={->true}
