.. _define_duplicate_product_lines:

Duplicate Product Lines
-----------------------

This page details our solution to the `Define Duplicate Product Lines <https://dmcommunity.org/challenge/challenge-august-2015/>`_ challenge from `dmcommunity.org <https://dmcommunity.org>`_

.. admonition:: Define Duplicate Product Lines

   Let’s assume that your organization handles sales orders similar to the one below:

    .. csv-table::
        :header: "Product SKU", "Price", "Quantity"

        "SKU-1000", "10", "10"
        "SKU-1", "20", "3"
        "SKU-2", "30", "1"
        "SKU-1", "20", "4"
        "SKU-3", "40", "6"
        "SKU-3", "42", "8"
        "SKU-4", "15", "2"

    Create a decision model that is capable to decide which product lines inside such orders are duplicate.
    The duplicate product lines can be recognized by user-defined similarity rules, e.g. two product lines are considered “duplicate” if they have the same “sku” and “price”.

To solve this challenge, we simply need to check whether two orders have the same properties.
To be able to model these properties, we first define them in our glossary.
We create a type for every product (one to seven), a type containing the possible SKU's, and a type representing the quantities.


.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Product</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[1..7]</td>
        </tr>
        <tr>
            <td class="glos-td">SKU</td>
            <td class="glos-td">String</td>
            <td class="glos-td">SKU1000, SKU1, SKU2, SKU3, SKU4</td>
        </tr>
        <tr>
            <td class="glos-td">Quantity</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..100]</td>
        </tr>
    </table>
    <br>

Because every product has exactly one SKU, Price and Quantity, we model these properties using functions.


.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">sku of Product</td>
            <td class="glos-td">SKU</td>
        </tr>
        <tr>
            <td class="glos-td">price of Product</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">quantity of Product </td>
            <td class="glos-td">Quantity</td>
        </tr>
    </table>
    <br>
    

We also need a way to express that two products are duplicates of each other.
For this purpose, we introduce a relation.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Product is duplicate of Product</td>
        </tr>
    </table>
    <br>

Now our glossary is complete, and we can start to add logic.
For this specific challenge, the logic is simple: if two products share the same SKU and Price, they are duplicates.
We express this in a table as follows.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="5">Define duplicates</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-input">Product called p1</td>
            <td class="dec-input">Product called p2</td>
            <td class="dec-input">sku of p1</td>
            <td class="dec-input">price of p1</td>
            <td class="dec-output">p1 is duplicate of p2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(p1)</td>
            <td class="dec-td">sku of p2</td>
            <td class="dec-td">price of p2</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

In other words, the table above states "For every Product p1 and p2 (not equal to p1), if they have the same SKU and the same Price, they are duplicates."
Or, "Duplicates are defined as two products that share the same SKU and Price".

Now, all that remains is add in our data table containing the product information.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Product</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Product</td>
            <td class="dec-output">sku of Product</td>
            <td class="dec-output">price of Product</td>
            <td class="dec-output">quantity of Product</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">1</td>
            <td class="dec-td">SKU1000</td>
            <td class="dec-td">10</td>
            <td class="dec-td">10</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">2</td>
            <td class="dec-td">SKU1</td>
            <td class="dec-td">20</td>
            <td class="dec-td">3</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">3</td>
            <td class="dec-td">SKU2</td>
            <td class="dec-td">30</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">4</td>
            <td class="dec-td">SKU1</td>
            <td class="dec-td">20</td>
            <td class="dec-td">4</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">5</td>
            <td class="dec-td">SKU3</td>
            <td class="dec-td">40</td>
            <td class="dec-td">6</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">6</td>
            <td class="dec-td">SKU3</td>
            <td class="dec-td">42</td>
            <td class="dec-td">8</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">7</td>
            <td class="dec-td">SKU4</td>
            <td class="dec-td">15</td>
            <td class="dec-td">2</td>
        </tr>
    </table>
    <br>


Running the solver using this model gives us the following solution.

.. code:: bash

   Model 1
   ==========
   sku_of_Product := {1->SKU1000, 2->SKU1, 3->SKU2, 4->SKU1, 5->SKU3, 6->SKU3, 7->SKU4}.
   price_of_Product := {1->10, 2->20, 3->30, 4->20, 5->40, 6->42, 7->15}.
   quantity_of_Product := {1->10, 2->3, 3->1, 4->4, 5->6, 6->8, 7->2}.
   Product_is_duplicate_of_Product := {(2,4), (4,2)}.

   Elapsed Time:
   0.871
