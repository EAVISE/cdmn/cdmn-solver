.. _zoo_buses_and_kids:

Zoo, Buses and Kids
-------------------

Zoo, Buses and Kids is a small optimization problem taken from the `DMCommunity challenges. <https://dmcommunity.org/challenge/challenge-july-2018/>`_

.. admonition:: Zoo, Buses and Kids

   300 kids need to travel to the London zoo.
   The school may rent 40 seats and 30 seats buses for 500 and 400 £.
   How many buses of each to minimize cost?

You can probably solve this problem without the use of a computer, but it's still a nice showcase of optimization within cDMN.

We start by creating a glossary.
We need a type to represent buses and number of kids.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Bus</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Big, Small</td>
        </tr>
        <tr>
            <td class="glos-td">Kid</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">[0..300]</td>
        </tr>
    </table>
    <br>

For every bus, we need to express exactly one price, and exactly one size.
Furthermore, we need a way to express how many buses of each type we will have, and how many children will be on each bus type.
Since all of these concepts map one type on exactly one type, we use functions for this purpose.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">price of Bus</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">size of Bus</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Number of Bus</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">Kids of Bus</td>
            <td class="glos-td">Kid</td>
        </tr>
    </table>
    <br>

To represent our total number of kids which need seating and the total price of all the buses, we use constants.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">TotalPrice</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TotalKids</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

The next step is inputting the data in our model.
We know for each bus type their size and their price, and we represent this in a data table.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Price and size of Buses</th>
            <th class="dec-empty" colspan="2"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Bus</td>
            <td class="dec-output">size of Bus</td>
            <td class="dec-output">price of Bus</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Big</td>
            <td class="dec-td">40</td>
            <td class="dec-td">500</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Small</td>
            <td class="dec-td">30</td>
            <td class="dec-td">400</td>
        </tr>
    </table>
    <br>

We also already know the total number of kids.


.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Total Kids</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">U</td>
            <td class="dec-output">TotalKids</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">300</td>
        </tr>
    </table>
    <br>

We now express two things in order to complete the logic:

1. Every kid needs a bus.
2. Buses cannot have a number of children exceeding their size.


This first rule is state by the following table.
It checks that the sum of the number of kids assigned to each bus type equals the total number of kids.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Every kid needs a bus.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Bus</td>
            <td class="dec-output">TotalKids</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Kids of Bus</td>
        </tr>
    </table>
    <br>

The second rule is expressed by a constraint.
For every type of bus, there cannot be more children than the size of the type multiplied by the number of the type.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Limit number of kids per bus.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Bus</td>
            <td class="dec-output">size of Bus * Number of Bus</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td"> &ge; Kids of Bus</td>
        </tr>
    </table>
    <br>

After implementing these two rules, we still need to add the formula for calculating the total price, and then tell the solver to minimize it by adding a ``Goal`` table.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Calc TotalPrice.</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Bus</td>
            <td class="dec-output">TotalPrice</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Number of Bus * price of Bus</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Minimize TotalPrice</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
This results in the following output:

.. code:: bash


   Model 1
   ==========
   price_of_Bus := {Big->500, Small->400}.
   size_of_Bus := {Big->40, Small->30}.
   number_of_Bus := {Big->6, Small->2}.
   kids_of_Bus := {Big->240, Small->60}.
   TotalPrice := 3800.
   TotalKids := 300.

   Elapsed Time:
   0.822

The optimal solution is to get 6 big buses, and 2 small ones for a total cost of 3800 pounds.
