.. _boats:

Boat Rental
-----------

The following problem was posed as the `DMCommunity September 2024 challenge. <https://dmcommunity.org/challenge-sep-2024/>`_

.. admonition:: Rental Boats

   Floataway tours has $420,000 that may be used to purchase new rental boats for hire during the summer. The boats can be purchased from two different manufacturers. Floataway tours would like to purchase at least 50 boats and would like to purchase the same number from Sleekboat as from Racer to maintain goodwill. Also, Floataway Tours wishes to have a total capacity of at least 200. Data about the boats is summarized below:

    .. raw:: html 

        <table>
            <tr>
                <th>Boat</th>
                <th>Manufacturer</th>
                <th>Cost</th>
                <th>Seating</th>
                <th>Expected Daily Profit</th>
            </tr>
            <tr>
                <td>Speedhawk</td>
                <td>Sleekboat</td>
                <td>6000</td>
                <td>3</td>
                <td>70</td>
            </tr>
            <tr>
                <td>Silverbird</td>
                <td>Sleekboat</td>
                <td>7000</td>
                <td>5</td>
                <td>80</td>
            </tr>
            <tr>
                <td>Catman</td>
                <td>Racer</td>
                <td>5000</td>
                <td>2</td>
                <td>50</td>
            </tr>
            <tr>
                <td>Classy</td>
                <td>Racer</td>
                <td>9000</td>
                <td>6</td>
                <td>110</td>
            </tr>
        </table> 


Can we solve this using cDMN? Let's find out.

As always, we want to start by creating a glossary (See :ref:`notation_glossary`).
For this challenge, we can clearly identify two types: boats, and their manufacturer.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Boat</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Speedhawk,  Silverbird, Catman, Classy</td>
        </tr>
        <tr>
            <td class="glos-td">Manufacturer</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Sleekboat, Racer</td>
        </tr>
    </table>
    <br>

We've also been given some information about the boats that we need to be able to represent: their manufacturer, their cost, their seating capacity, and their estimated profit.
These properties are best represented using a function, i.e., a mapping from one type on another.

Additionally, we also want a way to represent the number of each boat type that we buy, and the total of each boat type bought per manufacturer.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">manufacturer of Boat</td>
            <td class="glos-td">Manufacturer</td>
        </tr>
        <tr>
            <td class="glos-td">cost of Boat</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">seating of Boat</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">profit of Boat</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">nb of Boat</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">total of Manufacturer</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

Finally, we need a way to represent the three requirements (total boats, total capacity, total price) and a way to represent the total profit.
As these are just numerical values, we can represent them as constant variables.
Note that constant here does not mean that their value always needs to be known up front, but rather that they have exactly one value in each solution.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">total boats</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">total capacity</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">total price</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">total profit</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

That concludes our glossary!
We know have all the symbols that we need to start expressing rules.
The first piece of logic that we'll add is quite a simple one: we can only buy a positive number of boats.
Because our function `nb of Boat` maps each boat on an integer number, it would be possible to have negative boats without such a constraint.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Non-neg constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Boat</td>
            <td class="dec-output">nb of Boat</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">&ge;</td>
        </tr>
    </table>
    <br>

This table can be read as "The number of each boat should be greater than or equal to zero".

Next, we add the constraints on the total requirements: 

* minimum capacity of 200
* at least 50 boats
* max total price of 420000
* total Sleekboat is equal to total Racer

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Starting value</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">total capacity</td>
            <td class="dec-output">total boats</td>
            <td class="dec-output">total price</td>
            <td class="dec-output">total of Sleekboat</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&ge; 200</td>
            <td class="dec-td">&ge; 50</td>
            <td class="dec-td">&le; 420000</td>
            <td class="dec-td">total of Racer</td>
        </tr>
    </table>
    <br>

Of course, we still need to define how we calculate these values.
The total profit, capacity and price are luckily quite easy to calculate using a `C+` (count) table, as we just need to multiply the property of each boat with the number bought of each boat.
Even better, we can count all three of them together in the same table.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Count parameters</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Boat</td>
            <td class="dec-output">total profit</td>
            <td class="dec-output">total capacity</td>
            <td class="dec-output">total price</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">profit of Boat * nb of Boat</td>
            <td class="dec-td">seating of Boat * nb of Boat</td>
            <td class="dec-td">cost of Boat * nb of Boat</td>
        </tr>
    </table>
    <br>

Additionally, we also need to calculate the number of boats per manufacturer.
This table is similar to the previous one, but we will also need to "go over" (= quantify) each manufacturer as well.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Calculate number per manufacturer</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Boat</td>
            <td class="dec-input">Manufacturer</td>
            <td class="dec-output">total of Manufacturer</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">manufacturer of Boat</td>
            <td class="dec-td">nb of Boat</td>
        </tr>
    </table>
    <br>

In essence, this table will, for each manufacturer, count their number of boats.
Note that the cell `manufacturer of Boat` is crucial here, as this one ensures that only the boats by that manufacturer are counted.

And that's basically it for the business logic! All in all not that difficult.
All that remains is specifying the concrete boat data using a data table, and adding a goal table to express the optimization.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Boat info</th>
            <th class="dec-empty" colspan="1"></th>
            <th class="dec-empty" colspan="1"></th>
            <th class="dec-empty" colspan="1"></th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Boat</td>
            <td class="dec-output">manufacturer of Boat</td>
            <td class="dec-output">cost of Boat</td>
            <td class="dec-output">seating of Boat</td>
            <td class="dec-output">profit of Boat</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Speedhawk</td>
            <td class="dec-td">Sleekboat</td>
            <td class="dec-td">6000</td>
            <td class="dec-td">3</td>
            <td class="dec-td">70</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Silverbird</td>
            <td class="dec-td">Sleekboat</td>
            <td class="dec-td">7000</td>
            <td class="dec-td">5</td>
            <td class="dec-td">80</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Catman</td>
            <td class="dec-td">Racer</td>
            <td class="dec-td">5000</td>
            <td class="dec-td">2</td>
            <td class="dec-td">50</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">Classy</td>
            <td class="dec-td">Racer</td>
            <td class="dec-td">9000</td>
            <td class="dec-td">6</td>
            <td class="dec-td">110</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Maximize total profit</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver or `the online IDE <http://interactive-consultant.idp-z3.be/IDE?NohEHMBsHsGdYIYCcCeoA0Eb2WzU5FVQBddYUDUAFRQAcBTK080AOQQFsnMARBAC4JajKgAVCASwBGkBgAIAagkgBXBrBYUAQtEFUAygKSSAduEOMGAEwAWCAO4BrdPIOTIANwZJpkpNauAMKCnAimwZAI8GhkFACy4aoAZggAxgKqSD6GxmYWmAZyDE7SegKuAErpOSRxFAQ4xPjYRLGsAGKqphmS0KbMcexcPKD8QiJMQ2GmKemZ2Ujy0MnyuvqYibOpGVm1rGlwAsur6wJUAJKm50OwDIL5J2vll9daoHRIK5LHK88boCuN1YpmkTzOr2BFAE0CEkCeWzmu0WkNI9SwhFwVEabXeQX6sCEb0w7w43Co42E9CmrBhcPkZUEmkwQPedJU8jSCDo6R%2BeEBbyG7Phn0kaVGrKFsI5n2%2B5xZgvR8TM8myAEcqGY6Kp5aBoDrtVDQABRABUVAhmFB4JeQwAjFQALRUAB8AF55AAGNHkCjqgAS92sOUw%2BoEhqoYYjoYNOsjsaNZqows53N5An5KcZAmZoBTovFyel8L%2BRQYJWz7wdmHd8gATJ7vTWPQBWJugAA8HoALA3G%2B2U39quKkD6wCFIGlVFEBAoeUgRrOkLmtXGY%2BG13qE-GN0aggBqC0vTD5r7JH5F%2BlcnlpPmXmUmQv2p1UWXn36ncryU3ya1-S2gHcDzmDagjfr%2BYL-seoCHISoHHD%2Bf6fvodS%2BqAE5TjOCjWowSwzEiCwhqAq66iRO4RkMB5HgCiI7IRo4nsWCJJHReyjs%2BmDOpg%2BGsYs8FUEh-w3OiZzyFcyTQJqpjRluu7kZuUYKduQy8NRuo8fMbH8ZgsEfkJVBAQIjxQQCb4-PxHGgAYVh2I4TiGMUpTQQAbP2VAAMxUAA7N6Qx1oYHjeL4-jWA55ZOQCPmNlQLZUAAHL5rCeZgIQCDMVDDkRbbRZg-mYG27zdlQQRRDEGU1AxoAAJxuZgzlUHadq%2Bei4B6JA7wAOJte8YQAB6SJwkgAF4KKecpoiQQA&ed=cdmn>`_.
This results in the following output:

.. code:: bash

   Model 1
   ==========
   nb_of_Boat := {Speedhawk -> 28, Silverbird -> 0, Catman -> 0, Classy -> 28}.
   total_of_Manufacturer := {Sleekboat -> 28, Racer -> 28}.
   total_boats := 50.
   total_capacity := 252.
   total_price := 420000.
   total_profit := 5040.

   Elapsed Time:
   0.499

The optimal distribution is to buy 28 Speedhawk en 28 Classy, for a total estimated profit of 5040 dollar.

