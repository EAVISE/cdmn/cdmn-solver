.. _map_coloring:

Map Coloring
------------

The map coloring problem is a common problem.
We will show how to solve it using only cDMN.
The full DMCommunity challenge, with other submitted solutions can be found `here. <https://dmcommunity.org/challenge/challenge-may-2019/>`_

.. admonition:: Map Coloring Challenge


    This challenge deals with map coloring.
    You need to use no more than 4 colors (blue, red, green, or yellow) to color six European countries: Belgium, Denmark, France, Germany, Luxembourg, and the Netherlands in such a way that no neighboring countries use the same color. 


We start by creating our glossary.
For this specific problem, the glossary does not need to be complex.
We create a type to represent countries and a type to represent the colors.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Country</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Belgium, France, Germany, Luxembourg, Netherlands</td>
        </tr>
        <tr>
            <td class="glos-td">Color</td>
            <td class="glos-td">String</td>
            <td class="glos-td">Green, Red, Yellow, Orange</td>
        </tr>
    </table>
    <br>

To assign every country exactly one color, we can use a function.
To express that two countries border, we use a relation.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">color of Country</td>
            <td class="glos-td">Color</td>
        </tr>
    </table>
    <br>

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">Country borders Country</td>
        </tr>
    </table>
    <br>

Now that our glossary is done, we can move on to the next step: modeling the problem logic.
This logic is very simple: if two countries border, they cannot share the same color.
We can represent this straightforwardly using a constraint table.
The table is read as: "For every two countries c1, c2, if they border they cannot share the same color."

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Bordering countries cannot share colors</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Country called c1</td>
            <td class="dec-input">Country called c2</td>
            <td class="dec-input">c1 borders c2</td>
            <td class="dec-output">color of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Not(color of c2)</td>
        </tr>
    </table>
    <br>

And that is all the logic we need to solve this problem!
We simply need to represent which countries border which using a data table, and we're done.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Bordering Countries</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Country called c1</td>
            <td class="dec-input">Country called c2</td>
            <td class="dec-output">c1 borders c2</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">Belgium</td>
            <td class="dec-td">France, Luxembourg, Netherlands, Germany</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">Netherlands</td>
            <td class="dec-td">Germany</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">Germany</td>
            <td class="dec-td">France, Luxembourg, Denmark</td>
            <td class="dec-td">Yes</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">France</td>
            <td class="dec-td">Luxembourg</td>
            <td class="dec-td">Yes</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
This results in the following output:

.. code:: bash

   Model 1
   ==========
   color_of_Country := {Belgium->Yellow, France->Red, Luxembourg->Green, Netherlands->Green, Germany->Orange, Denmark->Green}.
   Country_borders_Country := {(Belgium,France), (Belgium,Luxembourg), (Belgium,Netherlands), (Belgium,Germany), (France,Luxembourg), (Netherlands,Germany), (Germany,France), (Germany,Luxembourg), (Germany,Denmark)}.

   Elapsed Time:
   0.902

We are now able to color a map without having to worry about bordering countries sharing a color!
