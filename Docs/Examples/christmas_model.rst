.. _christmas_model:

Christmas Model
---------------

DMCommunity's January 2023 challenge, called "`Christmas Model <https://dmcommunity.org/challenge-jan-2023/>`_", tasks us with finding the optimal assignment of gifts to people, given a specific budget.


.. admonition:: Christmas Model

   This model defines a set of people, a set of gifts, and the happiness level and cost of each gift. The objective is to maximize the total happiness, subject to the budget constraint that the total cost of the gifts must be less than or equal to the budget, and the constraint that each person can only receive one gift.

   Here is a sample of test data:

   * PEOPLE:  “Alice”, “Bob”, “Carol”, “Dave”, “Eve”
   * GIFTS: “Book”, “Toy”, “Chocolate”, “Wine”, “Flowers”
   * GIFT COSTS: 10, 20, 5, 15, 7
   * HAPPINESS:
   * “Book”: [3, 2, 5, 1, 4]
   * “Toy”: [5, 2, 4, 3, 1]
   * “Chocolate”: [1, 3, 4, 5, 2]
   * “Wine”: [2, 5, 3, 4, 1]
   * “Flowers”: [4, 3, 1, 2, 5]
   * BUDGET: 50

By analysing the prompt, we can see that we need to:

1. Count the total happiness.
2. Count the total cost.
3. Ensure the gifts fit within our budget.
4. Ensure each person only gets one gift.

Before we can do that though, we need to introduce the different symbols that we will be using by listing them in our glossary.
Firstly, we introduce our *types*, which, broadly speaking, represent domains of values.
In this problem, there are three such domains: *people*, *gifts*, and integers.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Person</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Alice, Bob, Carol, Dave, Eve</td>
        </tr>
        <tr>
            <td class="glos-td">Gift</td>
            <td class="glos-td">string</td>
            <td class="glos-td">Book, Toy, Chocolate, Wine, Flowers</td>
        </tr>
    </table>
    <br>

Note that integers are a standard type in cDMN, and do not need to explicitly be declared.

Next, we need a way to assign a gift to a person, a cost to each gift and a happiness to each (Person, Gift) pair.
One thing these three concepts have in common is that they each represent a mapping of one or more types on a single type.
In cDMN, we can model these using functions.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">DataType</td>
        </tr>
        <tr>
            <td class="glos-td">gift of Person</td>
            <td class="glos-td">Gift</td>
        </tr>
        <tr>
            <td class="glos-td">cost of Gift</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">happiness of Person getting Gift</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

For example, the function *gift of Person* has one argument, *Person*, and maps it on type *Gift*.
Because each person will be mapped on exactly one gift, this representation already adheres to requirement 4 from above.

Lastly, we need a symbol to represent the total amount of happiness, and the total cost.
Here, we can use constants, i.e., symbols that have exactly one value.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">TotalHappiness</td>
            <td class="glos-td">Int</td>
        </tr>
        <tr>
            <td class="glos-td">TotalCost</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

Now that are glossary is finished, we can enter the data which we know up front.
In this case, we already know two things: the cost of each item, and the happiness of each item for each person.
In cDMN, we enter these via *Data Tables*.



.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Cost</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Gift</td>
            <td class="dec-output">cost of Gift</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Book</td>
            <td class="dec-td">10</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Toy</td>
            <td class="dec-td">20</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Chocolate</td>
            <td class="dec-td">5</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Wine</td>
            <td class="dec-td">15</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Flowers</td>
            <td class="dec-td">7</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Happiness</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">Gift</td>
            <td class="dec-output">happiness of Person gettin Gift</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Alice</td>
            <td class="dec-td">Book</td>
            <td class="dec-td">3</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Alice</td>
            <td class="dec-td">Toy</td>
            <td class="dec-td">5</td>
        </tr>
        <tr>
            <td class="dec-td"></td>
            <td class="dec-td">Alice</td>
            <td class="dec-td">Chocolate</td>
            <td class="dec-td">1</td>
        </tr>
        <tr>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
            <td class="dec-td">...</td>
        </tr>
    </table>
    <br>

Now that that's done, we can focus on modelling our three remaining requirements.

.. admonition:: R1 & R2

    **Count the total happiness and the total cost.**

To count the total happiness, we simply count the individual happiness of each person depending on what gift they're getting.
Similarly, for the gift, we count the individual cost of each gift that is given to a person.
We can model this in the following C+ (Sum) table:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="4">Count happiness and cost</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Person</td>
            <td class="dec-input">Gift</td>
            <td class="dec-input">gift of Person</td>
            <td class="dec-output">TotalHappiness</td>
            <td class="dec-output">TotalCost</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Gift</td>
            <td class="dec-td">happiness of Person getting Gift</td>
            <td class="dec-td">cost of Gift</td>
        </tr>
    </table>
    <br>

"*Sum the happiness of each Person if they are getting Gift.*" and "*Sum the cost of each Gift that has been given to a Person.*"

.. admonition:: R3

    **Ensure the gifts fit within our budget.**


Our last requirement is expressing the constraint that we must not go over budget.
This is very straightforwardly expressed in a constraint (E*) table.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">Budget</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">TotalCost</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">&le; 50</td>
        </tr>
    </table>
        <br>

.. admonition:: Goal
   
   **Maximize total happiness**

Finally, we want to express that our goal is to optimize the total happiness.

.. raw:: html

    <table class="exe">
        <tr>
            <th class="exe-title">Goal</th>
        </tr>
        <tr>
            <td class="exe-td">Maximize TotalHappiness </td>
        </tr>
    </table>
    <br>


Now that our cDMN specification is finished, we can run the solver to get the optimal gift assignment!
This results in the following solution:
 
.. code:: bash

    Model 1
    ==========
    gift_of_Person := {Alice -> Flowers, Bob -> Wine, Carol -> Book, Dave -> Chocolate, Eve -> Flowers}.
    happiness_of_Person_getting_Gift := {(Alice, Book) -> 3, (Alice, Toy) -> 5, (Alice, Chocolate) -> 1, (Alice, Wine) -> 2, (Alice, Flowers) -> 4, (Bob, Book) -> 2, (Bob, Toy) -> 2, (Bob, Chocolate) -> 3, (Bob, Wine) -> 5, (Bob, Flowers) -> 3, (Carol, Book) -> 5, (Carol, Toy) -> 4, (Carol, Chocolate) -> 4, (Carol, Wine) -> 3, (Carol, Flowers) -> 1, (Dave, Book) -> 1, (Dave, Toy) -> 3, (Dave, Chocolate) -> 5, (Dave, Wine) -> 4, (Dave, Flowers) -> 2, (Eve, Book) -> 4, (Eve, Toy) -> 1, (Eve, Chocolate) -> 2, (Eve, Wine) -> 1, (Eve, Flowers) -> 5}.
    cost_of_Gift := {Book -> 10, Toy -> 20, Chocolate -> 5, Wine -> 15, Flowers -> 7}.
    TotalHappiness := 24.
    TotalCost := 44.

    Elapsed Time:
    0.797s

With a total happiness of 24, the optimal assignment is as follows:

* Alice: Flowers
* Bob: Wine
* Carol: Book
* Dave: Chocolate
* Eve: Flowers

And with a total cost of 44, we still have enough money left to buy ourselves some chocolates too. :-)
