.. _set:

SET
---

SET is a `board game published by Set Enterprises, Inc <https://boardgamegeek.com/boardgame/1198/set>`_, in which the goal is to find a *set* of cards.
Concretely, the rules are as follows:

.. admonition:: SET

    12 cards are placed on the table, each with four attributes:

    1. The symbol on the card (diamonds, squiggles, rectangles)
    2. The number of symbols (one, two, three)
    3. The color of the symbols (red, green or purple)
    4. The fill of the symbols (striped, filled, empty)

    The goal is now to find a set of **three** cards, that either have the same value or a different value for every attribute.

    For example, a possible set in the image below would be the most top-left card together with the two bottom-right cards, as they each have:
    
    * The same symbol (diamonds)
    * The same number (2)
    * Different colors
    * Different fills

.. image:: ../_static/set.jpg
   :width: 600
   :alt: an example of SET cards


First, we start building the glossary of the model.
There are a few types that should be declared:

* Card (we will use indices 1 through 12)
* Color
* Number
* Shape
* Fill

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="3">Type</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
            <td class="glos-header">Values</td>
        </tr>
        <tr>
            <td class="glos-td">Card</td>
            <td class="glos-td">Int</td>
            <td class="glos-td">1..12</td>
        </tr>
        <tr>
            <td class="glos-td">Color</td>
            <td class="glos-td">String</td>
            <td class="glos-td">red, green, purple</td>
        </tr>
        <tr>
            <td class="glos-td">Number</td>
            <td class="glos-td">String</td>
            <td class="glos-td">one, two, three</td>
        </tr>
        <tr>
            <td class="glos-td">Shape</td>
            <td class="glos-td">String</td>
            <td class="glos-td">diamonds, squiggles, rectangles</td>
        </tr>
        <tr>
            <td class="glos-td">Fill</td>
            <td class="glos-td">String</td>
            <td class="glos-td">striped, filled, empty</td>
        </tr>
    </table>
    <br>

Next, we want to add a way to link card indices to card attributes.
For this, functions are perfect: every card has exactly one value for every attribute.
The concrete values for these functions will be filled out later in a data table.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Function</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">color of Card</td>
            <td class="glos-td">Color</td>
        </tr>
        <tr>
            <td class="glos-td">number of Card</td>
            <td class="glos-td">Number</td>
        </tr>
        <tr>
            <td class="glos-td">fill of Card</td>
            <td class="glos-td">Fill</td>
        </tr>
        <tr>
            <td class="glos-td">shape of Card</td>
            <td class="glos-td">Shape</td>
        </tr>
    </table>
    <br>

We also need a way to keep track of what cards are selected to be in our set, and if they have the same/different color, same/different number, ...
We will use a relation for the former, and booleans for the latter (as these are either the same, or different).

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Relation</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">selected Card</td>
        </tr>
    </table>
    <br>

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="1">Boolean</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
        </tr>
        <tr>
            <td class="glos-td">SameColor</td>
        </tr>
        <tr>
            <td class="glos-td">SameNumber</td>
        </tr>
        <tr>
            <td class="glos-td">SameShape</td>
        </tr>
        <tr>
            <td class="glos-td">SameFill</td>
        </tr>
    </table>
    <br>

Finally, to keep track of how many cards have been selected by the system, we introduce a constant.

.. raw:: html 

    <table class="glos">
        <tr>
            <th class="glos-title" colspan="2">Constant</th>
        </tr>
        <tr>
            <td class="glos-header">Name</td>
            <td class="glos-header">Type</td>
        </tr>
        <tr>
            <td class="glos-td">NbCards</td>
            <td class="glos-td">Int</td>
        </tr>
    </table>
    <br>

Now that our glossary is done, we can move on to implementing the rules.
These are actually quite straight forward.
For every attribute, we want to say the following:

.. admonition:: Rule

   **For every two cards that are selected, if SameAttribute is true, their attribute values should be the same.
   Similarly, if SameAttribute is false, their attribute values should be different.**

Concretely, this is implemented in the following constraint tables:

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">Color constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Card called c1</td>
            <td class="dec-input">Card called c2</td>
            <td class="dec-input">selected c1</td>
            <td class="dec-input">selected c2</td>
            <td class="dec-input">SameColor</td>
            <td class="dec-output">color of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">color of c2</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
            <td class="dec-td">not(color of c2)</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">Number constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Card called c1</td>
            <td class="dec-input">Card called c2</td>
            <td class="dec-input">selected c1</td>
            <td class="dec-input">selected c2</td>
            <td class="dec-input">SameNumber</td>
            <td class="dec-output">number of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">number of c2</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
            <td class="dec-td">not(number of c2)</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">Fill constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Card called c1</td>
            <td class="dec-input">Card called c2</td>
            <td class="dec-input">selected c1</td>
            <td class="dec-input">selected c2</td>
            <td class="dec-input">SameFill</td>
            <td class="dec-output">fill of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">fill of c2</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
            <td class="dec-td">not(fill of c2)</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="6">Shape constraint</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-input">Card called c1</td>
            <td class="dec-input">Card called c2</td>
            <td class="dec-input">selected c1</td>
            <td class="dec-input">selected c2</td>
            <td class="dec-input">SameShape</td>
            <td class="dec-output">shape of c1</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">shape of c2</td>
        </tr>
        <tr>
            <td class="dec-td">2</td>
            <td class="dec-td">-</td>
            <td class="dec-td">not(c1)</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">No</td>
            <td class="dec-td">not(shape of c2)</td>
        </tr>
    </table>
    <br>

The *shape constraint* table, for example, reads as follows: "For every Card c1 and Card c2 (that isn't c1), if both c1 and c2 are selected and SameShape is true, then their shape should be the same. If SameShape is false, their shape should be different."


To finish up, we want to make sure that a set always consists of three selected cards.
For that, we add a table which counts the number of selected cards, followed by a constraint on that number.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="3">Count number of selected cards</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">C+</td>
            <td class="dec-input">Card</td>
            <td class="dec-input">selected Card</td>
            <td class="dec-output">NbCards</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">-</td>
            <td class="dec-td">Yes</td>
            <td class="dec-td">1</td>
        </tr>
    </table>
    <br>

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="1">3 cards</th>
            <th class="dec-empty"></th>
        </tr>
        <tr>
            <td class="dec-td">E*</td>
            <td class="dec-output">NbCards</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">3</td>
        </tr>
    </table>
    <br>

Having entered all the rules, all that rests is filling out our data table.
We use the example shown in the image above here.

.. raw:: html

    <table class="dec">
        <tr>
            <th class="dec-title" colspan="2">Card Data</th>
            <th class="dec-empty" colspan="1"></th>
        </tr>
        <tr>
            <td class="dec-td">D</td>
            <td class="dec-input">Card</td>
            <td class="dec-output">number of Card</td>
            <td class="dec-output">color of Card</td>
            <td class="dec-output">fill of Card</td>
            <td class="dec-output">shape of Card</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">1</td>
            <td class="dec-td">two</td>
            <td class="dec-td">red</td>
            <td class="dec-td">striped</td>
            <td class="dec-td">diamonds</td>
        </tr>
        <tr>
            <td class="dec-td">1</td>
            <td class="dec-td">2</td>
            <td class="dec-td">one</td>
            <td class="dec-td">purple</td>
            <td class="dec-td">filled</td>
            <td class="dec-td">diamonds</td>
        </tr>
        <tr>
            <td class="dec-td">3</td>
            <td class="dec-td">3</td>
            <td class="dec-td">two</td>
            <td class="dec-td">green</td>
            <td class="dec-td">filled</td>
            <td class="dec-td">rectangles</td>
        </tr>
        <tr>
            <td class="dec-td">4</td>
            <td class="dec-td">4</td>
            <td class="dec-td">one</td>
            <td class="dec-td">green</td>
            <td class="dec-td">striped</td>
            <td class="dec-td">rectangles</td>
        </tr>
        <tr>
            <td class="dec-td">5</td>
            <td class="dec-td">5</td>
            <td class="dec-td">two</td>
            <td class="dec-td">red</td>
            <td class="dec-td">filled</td>
            <td class="dec-td">rectangles</td>
        </tr>
        <tr>
            <td class="dec-td">6</td>
            <td class="dec-td">6</td>
            <td class="dec-td">three</td>
            <td class="dec-td">red</td>
            <td class="dec-td">empty</td>
            <td class="dec-td">squiggles</td>
        </tr>
        <tr>
            <td class="dec-td">7</td>
            <td class="dec-td">7</td>
            <td class="dec-td">one</td>
            <td class="dec-td">red</td>
            <td class="dec-td">empty</td>
            <td class="dec-td">diamonds</td>
        </tr>
        <tr>
            <td class="dec-td">8</td>
            <td class="dec-td">8</td>
            <td class="dec-td">one</td>
            <td class="dec-td">purple</td>
            <td class="dec-td">striped</td>
            <td class="dec-td">rectangles</td>
        </tr>
        <tr>
            <td class="dec-td">9</td>
            <td class="dec-td">9</td>
            <td class="dec-td">two</td>
            <td class="dec-td">red</td>
            <td class="dec-td">empty</td>
            <td class="dec-td">rectangles</td>
        </tr>
        <tr>
            <td class="dec-td">10</td>
            <td class="dec-td">10</td>
            <td class="dec-td">three</td>
            <td class="dec-td">purple</td>
            <td class="dec-td">striped</td>
            <td class="dec-td">squiggles</td>
        </tr>
        <tr>
            <td class="dec-td">11</td>
            <td class="dec-td">11</td>
            <td class="dec-td">two</td>
            <td class="dec-td">purple</td>
            <td class="dec-td">filled</td>
            <td class="dec-td">diamonds</td>
        </tr>
        <tr>
            <td class="dec-td">12</td>
            <td class="dec-td">12</td>
            <td class="dec-td">two</td>
            <td class="dec-td">green</td>
            <td class="dec-td">empty</td>
            <td class="dec-td">diamonds</td>
        </tr>
    </table>
    <br>

We can now run our model using the cDMN solver.
In total, we are able to find 5 sets!

.. code:: bash

    Model 1
    ==========
    color_of_Card:={(1)->red; (2)->purple; (3)->green; (4)->green; (5)->red; (6)->red; (7)->red; (8)->purple; (9)->red; (10)->purple; (11)->purple; (12)->green}
    number_of_Card:={(1)->two; (2)->one; (3)->two; (4)->one; (5)->two; (6)->three; (7)->one; (8)->one; (9)->two; (10)->three; (11)->two; (12)->two}
    fill_of_Card:={(1)->striped; (2)->filled; (3)->filled; (4)->striped; (5)->filled; (6)->empty; (7)->empty; (8)->striped; (9)->empty; (10)->striped; (11)->filled; (12)->empty}
    shape_of_Card:={(1)->diamonds; (2)->diamonds; (3)->rectangles; (4)->rectangles; (5)->rectangles; (6)->squiggles; (7)->diamonds; (8)->rectangles; (9)->rectangles; (10)->squiggles; (11)->diamonds; (12)->diamonds}
    NbCards:={->3}
    selected_Card:={(1)->true; (2)->false; (3)->false; (4)->false; (5)->false; (6)->false; (7)->false; (8)->false; (9)->false; (10)->false; (11)->true; (12)->true}
    SameColor:={->false}
    SameNumber:={->true}
    SameShape:={->true}
    SameFill:={->false}

    Model 2
    ==========
    color_of_Card:={(1)->red; (2)->purple; (3)->green; (4)->green; (5)->red; (6)->red; (7)->red; (8)->purple; (9)->red; (10)->purple; (11)->purple; (12)->green}
    number_of_Card:={(1)->two; (2)->one; (3)->two; (4)->one; (5)->two; (6)->three; (7)->one; (8)->one; (9)->two; (10)->three; (11)->two; (12)->two}
    fill_of_Card:={(1)->striped; (2)->filled; (3)->filled; (4)->striped; (5)->filled; (6)->empty; (7)->empty; (8)->striped; (9)->empty; (10)->striped; (11)->filled; (12)->empty}
    shape_of_Card:={(1)->diamonds; (2)->diamonds; (3)->rectangles; (4)->rectangles; (5)->rectangles; (6)->squiggles; (7)->diamonds; (8)->rectangles; (9)->rectangles; (10)->squiggles; (11)->diamonds; (12)->diamonds}
    NbCards:={->3}
    selected_Card:={(1)->false; (2)->false; (3)->false; (4)->true; (5)->false; (6)->true; (7)->false; (8)->false; (9)->false; (10)->false; (11)->true; (12)->false}
    SameColor:={->false}
    SameNumber:={->false}
    SameShape:={->false}
    SameFill:={->false}

    Model 3
    ==========
    color_of_Card:={(1)->red; (2)->purple; (3)->green; (4)->green; (5)->red; (6)->red; (7)->red; (8)->purple; (9)->red; (10)->purple; (11)->purple; (12)->green}
    number_of_Card:={(1)->two; (2)->one; (3)->two; (4)->one; (5)->two; (6)->three; (7)->one; (8)->one; (9)->two; (10)->three; (11)->two; (12)->two}
    fill_of_Card:={(1)->striped; (2)->filled; (3)->filled; (4)->striped; (5)->filled; (6)->empty; (7)->empty; (8)->striped; (9)->empty; (10)->striped; (11)->filled; (12)->empty}
    shape_of_Card:={(1)->diamonds; (2)->diamonds; (3)->rectangles; (4)->rectangles; (5)->rectangles; (6)->squiggles; (7)->diamonds; (8)->rectangles; (9)->rectangles; (10)->squiggles; (11)->diamonds; (12)->diamonds}
    NbCards:={->3}
    selected_Card:={(1)->false; (2)->false; (3)->false; (4)->false; (5)->false; (6)->true; (7)->true; (8)->false; (9)->true; (10)->false; (11)->false; (12)->false}
    SameColor:={->true}
    SameNumber:={->false}
    SameShape:={->false}
    SameFill:={->true}

    Model 4
    ==========
    color_of_Card:={(1)->red; (2)->purple; (3)->green; (4)->green; (5)->red; (6)->red; (7)->red; (8)->purple; (9)->red; (10)->purple; (11)->purple; (12)->green}
    number_of_Card:={(1)->two; (2)->one; (3)->two; (4)->one; (5)->two; (6)->three; (7)->one; (8)->one; (9)->two; (10)->three; (11)->two; (12)->two}
    fill_of_Card:={(1)->striped; (2)->filled; (3)->filled; (4)->striped; (5)->filled; (6)->empty; (7)->empty; (8)->striped; (9)->empty; (10)->striped; (11)->filled; (12)->empty}
    shape_of_Card:={(1)->diamonds; (2)->diamonds; (3)->rectangles; (4)->rectangles; (5)->rectangles; (6)->squiggles; (7)->diamonds; (8)->rectangles; (9)->rectangles; (10)->squiggles; (11)->diamonds; (12)->diamonds}
    NbCards:={->3}
    selected_Card:={(1)->true; (2)->false; (3)->false; (4)->true; (5)->false; (6)->false; (7)->false; (8)->false; (9)->false; (10)->true; (11)->false; (12)->false}
    SameColor:={->false}
    SameNumber:={->false}
    SameShape:={->false}
    SameFill:={->true}

    Model 5
    ==========
    color_of_Card:={(1)->red; (2)->purple; (3)->green; (4)->green; (5)->red; (6)->red; (7)->red; (8)->purple; (9)->red; (10)->purple; (11)->purple; (12)->green}
    number_of_Card:={(1)->two; (2)->one; (3)->two; (4)->one; (5)->two; (6)->three; (7)->one; (8)->one; (9)->two; (10)->three; (11)->two; (12)->two}
    fill_of_Card:={(1)->striped; (2)->filled; (3)->filled; (4)->striped; (5)->filled; (6)->empty; (7)->empty; (8)->striped; (9)->empty; (10)->striped; (11)->filled; (12)->empty}
    shape_of_Card:={(1)->diamonds; (2)->diamonds; (3)->rectangles; (4)->rectangles; (5)->rectangles; (6)->squiggles; (7)->diamonds; (8)->rectangles; (9)->rectangles; (10)->squiggles; (11)->diamonds; (12)->diamonds}
    NbCards:={->3}
    selected_Card:={(1)->false; (2)->false; (3)->true; (4)->false; (5)->false; (6)->false; (7)->true; (8)->false; (9)->false; (10)->true; (11)->false; (12)->false}
    SameColor:={->false}
    SameNumber:={->false}
    SameShape:={->false}
    SameFill:={->false}

    No more models.
