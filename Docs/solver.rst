.. _solver:

cDMN Solver
===========

.. note:: We are working on a tool that lets you interact with cDMN directly, without installation. `A preview version is available for free <http://cdmn-editor.simonvandevelde.be/>`_.

To be able to execute a spreadsheet, you need to use the cDMN solver.
The cDMN solver consists of two main parts:

1. A Python converter from cDMN to IDP language;
2. The `IDP-Z3 system <https://www.IDP-Z3.be>`_, a knowledge reasoning engine (can be run online or downloaded via Pip).


.. _solver_installation:

1. Installation
---------------

Installing the solver is easy via the Python repositories:

.. code:: bash

    $ pip3 install cdmn

The full source code for the solver is available at our `GitLab repository <https://gitlab.com/EAVISE/cdmn/cdmn-solver>`_.
It has been tested with Python 3.7=<. Other versions will probably work, but there's no guarantee.
The list of dependencies is as follows:

    * openpyxl
    * ply
    * numpy
    * python-dateutil
    * idp-engine



.. _solver_usage:

2. Usage
--------

The main usage is as follows:

.. code:: bash

    usage: cdmn [-h] [--version] [-n name_of_sheet [name_of_sheet ...]] [-o outputfile] [--idp idp] [--idp-z3] [--interactive-consultant] [--main]
                     [--errorcheck-overlap overlaptable] [--errorcheck-shadowed shadowedtable] [--errorcheck-rule ERRORCHECK_RULE]
                     [--errorcheck-gap ERRORCHECK_GAP]
                     path_to_file
    
    Run cDMN on DMN tables.
    
    positional arguments:
      path_to_file          the path to the xlsx or xml file
    
    options:
      -h, --help            show this help message and exit
      --version, -v         show program's version number and exit
      -n name_of_sheet [name_of_sheet ...], --name name_of_sheet [name_of_sheet ...]
                            the name(s) of the sheet(s) to execute
      -o outputfile, --outputfile outputfile
                            the name of the outputfile
      --idp idp             the path to the idp executable
      --idp-z3
      --interactive-consultant
                            generate file specifically for the Interactive Consultant
      --main                create a main, to use when generating for the IDP-Z3 Interactive Consultant
      --errorcheck-overlap overlaptable
                            the table to check for overlap errors: table is identified by table id
      --errorcheck-shadowed shadowedtable
                            the table to check for shadowed rules: table is identified by table id
      --errorcheck-rule ERRORCHECK_RULE
                            the rule to check for being erronous
      --errorcheck-gap ERRORCHECK_GAP
                            the table to check for input gaps: table is identified by table id



For example, to execute the sheet named ``Problem1`` in the ``.xlsx`` file located at ``Data/DMNImplementation.xlsx``, the following command is used.

.. code:: bash
    
    $ cdmn Data/DMNImplementation.xlsx -n "Problem1" -o "~/idp/Files/"


This will convert a sheet called ``Problem1`` from the ``DMNImplementation.xlsx`` file and save it in as ``~/idp/Files/Problem1.idp``.
It's also possible to set the name of the idp name yourself, by changing the output to ``~/idp/Files/custom_name.idp``.

If you wish to immediately execute the IDP specification, you can do so by supplying the ``--idp-z3`` flag.
This will automatically run the IDP-Z3 system, and print its output.

.. code:: bash

   $ cdmn Data/DMNImplementation.xlsx -n "Problem1" -o "problem.idp" --idp-z3

Some cDMN specification can get large, so sometimes it makes sense to create a logical separation in different sheets.
It is possible to run multiple sheets together by supplying multiple names after the ``-n`` flag.
For example:

.. code:: bash

   $ cdmn Examples/DMChallenges.xlsx -n "Balanced_Assignment" "Balanced_Assignment_Datatable

Alternatively, if your cDMN model is spread over all sheets in the table, you
can use the ``--all-sheets`` flag.

.. code:: bash

   $ cdmn Examples/set.xlsx --all-sheets

Create a file for the Interactive Consultant interface
______________________________________________________

The Interactive Consultant interface is an IDP-based, user-friendly interface which allows for straightforward interaction with a cDMN model.
In order to create a file that can be used in the interface, the ``interactive-consultant-idp-z3`` flag can be used.

.. code:: bash
   
   $ cdmn Data/DMNImplementation.xlsx -n "Problem1" -o "problem.idp" --interactive-consultant-idp-z3

In the case that a file is being generated for use in the IDP-Z3 webIDE, the ``--main`` flag should also be supplied.

.. note::

   If your cDMN tables contain annotations, these will also show up in your IDP-Z3 file.
   This makes the Interactive Consultant more explainable, so make full use of it!

Use DMN as input
________________

If you wish to execute DMN XML instead of a spreadsheet, you can do so by simply supplying the XML file as input.

Otherwise, it is also possible to create DMN tables inside a spreadsheet.
In this case however, it is still necessary to create a glossary for the DMN specification.
This can easily be done by filling out the Constant glossary table with all the variable names.
