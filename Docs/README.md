# cDMN

## Welcome to the cDMN solver's documentation repository.

cDMN stands for Constraint Decision and Model Notation.
It is an extension to the [DMN](https://www.omg.org/spec/DMN/About-DMN/) standard, managed by the Object Management Group (OMG).
cDMN combines the readability and straighforwardness of DMN and the expressiveness and flexibility of constraint reasoning.
For more specific details, please visit our [cDMN documentation](https://cdmn.readthedocs.io/en/latest/notation.html).

## Examples

Example implementations can also be found in the [cDMN documentation](https://cdmn.readthedocs.io/en/latest/examples.html).

## Installation and usage

The full installation and usage guide for the cDMN solver can be found [here](https://cdmn.readthedocs.io/en/latest/solver.html).

Percentage of working sheets: 100% (20/20)

List of Working sheets:

| Sheet  | Working?  | Latest Issue  |
|---|:-:|--:|
| Who killed Agatha  | ✓ | |
| Change making Decision | ✓ | |
| Make a Good Burger| ✓ | |
| Define Duplicate Product Lines | ✓ | |
| Collection of Cars | ✓ | |
| Monkey Business  | ✓ |  |
| Vacation days  | ✓ | |
| Family Riddle  | ✓ | |
| Greeting a Customer| ✓ |
| Online Dating Services | ✓ | |
| Classify Department Employees  | ✓ | |
| Reindeer Ordering  | ✓ |   |
| Zoo, Buses and Kids  | ✓ | |
| Balanced Assignment  | ✓ | |
| Vacation days advanced  | ✓ | |
| Map Coloring  | ✓ |   |
| Map Coloring Violations  | ✓ |
| Crack the Code  | ✓ | |
| Equation Haiku  | ✓ | |
| NIM Rules  | ✓ | |
