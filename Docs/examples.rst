.. _examples:

Examples
========

This page lists some examples for cDMN implementations.
For now, most examples are based on `DMCommunity challenges <https://dmcommunity.org/challenge/>`_.
In the future, other examples will be added as well.

The following table has a link for every example, together with a short list of the used concepts in the example.

.. csv-table:: cDMN Examples
   :header: "Example", "Source", "Concepts"

    :ref:`who_killed_agatha`, "https://dmcommunity.org/challenge/challenge-nov-2014/", "Quantification, Constraints, Functions",
    :ref:`vacation_days`, "https://dmcommunity.wordpress.com/challenge/challenge-jan-2016/","Quantification, Functions, Relations"
    :ref:`vacation_days_advanced`, "https://dmcommunity.org/challenge/challenge-nov-2018/", "Quantification, Functions, Relations, Data Table, Optimization"
    :ref:`hamburger_challenge`, "https://dmcommunity.org/challenge/make-a-good-burger/",  "Quantification, Constraints, Data Table, Functions, Constants, Optimization"
    :ref:`monkey_business`, "https://dmcommunity.org/challenge/challenge-nov-2015/", "Quantification, Constraints, Functions"
    :ref:`change_making`, "https://dmcommunity.org/challenge/challenge-feb-2015/", "Quantification, Constants, Optimization"
    :ref:`doctor_planning`, "https://dmcommunity.org/challenge/challenge-apr-2020/", "Quantification, Constraints, Functions, Relations, Optimization, Data Table"
    :ref:`balanced_assignment`, "https://dmcommunity.org/challenge/challenge-sep-2018/", "Quantification, Functions"
    :ref:`map_coloring`, "https://dmcommunity.org/challenge/challenge-may-2019/", "Quantification, Constraints, Functions, Relations, Data Table"
    :ref:`map_coloring_advanced`, "https://dmcommunity.org/challenge-june-2019/", "Quantification, Constraints, Functions, Relations, Data Table, Optimization"
    :ref:`crack_the_code`, "https://dmcommunity.org/challenge/challenge-sep-2019/", "Quantification, Constraints, Functions, Constants, Complex Maths"
    :ref:`zoo_buses_and_kids`, "https://dmcommunity.org/challenge/challenge-july-2018/", "Quantification, Constraints, Functions, Constants, Optimization"
    :ref:`define_duplicate_product_lines`, "https://dmcommunity.org/challenge/challenge-august-2015/", "Quantification, Functions, Relation, Data Table"
    :ref:`calculator`, "https://dmcommunity.org/challenge/challenge-nov-2020/", "Quantification, Functions, Constants, Optimization"
    :ref:`virtual_chess_tournament`, "https://dmcommunity.org/challenge/challenge-dec-2020/", "Quantification, Functions, Constraints, Aggregates"
    :ref:`set`, Joost Vennekens, "Quantification, Functions, Relations, Data Table, Goal Table, Constraints, Aggregates"
    :ref:`covid_testing`, "https://dmcommunity.org/challenge/challenge-may-2021/", "Booleans, Constants, Aggregates"
    :ref:`where_is_gold`, "https://dmcommunity.org/challenge/challenge-june-2021/", "Relations, Constants, Aggregates"
    :ref:`department_employees`, "https://dmcommunity.org/challenge/challenge-sep-2017/", "Functions, Constants, Relations, Aggregates, Quantification"
    :ref:`christmas_model`, "https://dmcommunity.org/challenge-jan-2023/", 
    :ref:`miss_manners`, "https://dmcommunity.org/challenge/challenge-june-2023/", "Functions, Constants, Relations, Aggregates, Quantification"
    :ref:`smart_investment`, "https://dmcommunity.org/challenge-july-2024/", "Functions, Constants, Aggregates, Quantification, Optimization"
    :ref:`boats`, "https://dmcommunity.org/challenge-sep-2024/", "Functions, Constants, Aggregates, Quantification, Optimization"

Full implementations
--------------------

.. toctree::
   :maxdepth: 3
   :caption: Contents: 

   Examples/monkey_business
   Examples/change_making
   Examples/who_killed_agatha
   Examples/hamburger_challenge
   Examples/vacation_days
   Examples/vacation_days_advanced
   Examples/doctor_planning
   Examples/balanced_assignment
   Examples/map_coloring
   Examples/map_coloring_advanced
   Examples/crack_the_code
   Examples/zoo_buses_and_kids
   Examples/define_duplicate_product_lines
   Examples/calculator
   Examples/virtual_chess_tournament
   Examples/set
   Examples/covid_testing
   Examples/where_is_gold
   Examples/department_employees
   Examples/christmas_model
   Examples/miss_manners
   Examples/smart_investment
   Examples/boats

